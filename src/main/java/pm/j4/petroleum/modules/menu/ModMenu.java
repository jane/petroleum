package pm.j4.petroleum.modules.menu;

import java.util.*;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import org.lwjgl.glfw.GLFW;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.Module;
import pm.j4.kerosene.util.data.ModInfoProvider;
import pm.j4.kerosene.util.module.ModuleFunction;
import pm.j4.kerosene.util.module.option.ConfigurationOption;
import pm.j4.petroleum.gui.PModMenuScreen;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.petroleum.util.config.ButtonPositionConfiguration;
import pm.j4.petroleum.util.data.ButtonInformation;
import pm.j4.kerosene.util.data.Category;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Mod menu.
 */
public class ModMenu extends ModuleBase {

	/**
	 * The constant coordinates.
	 */
	private static final Map<String, ButtonInformation> coordinates = new HashMap<>();

	/**
	 * Instantiates a new Mod menu.
	 */
	public ModMenu() {
		super("petroleum",
				"petroleum.modmenu",
				"petroleum.misc",
				true,
				true,
				false);
	}

	// TODO figure out resizing
	//  the number itself changes, so it should just be probably like some onResize bullshit in PModMenuScreen
	@Override
	public void init() {
		Optional<ConfigHolder> config = ConfigManager.getConfig(this.getParent());
		Map<String, List<ModuleBase>> categories = Category.getCategoryMap();
		final double[] h = {.1};
		Optional<ConfigurationOption> buttonOption = this.getConfigOption("petroleum.modmenu.buttons");
		categories.forEach((category, moduleList) -> {
			ButtonInformation conf = buttonOption.isPresent() ?
					((ButtonPositionConfiguration)buttonOption.get()).getButton(category) :
					null;
			ButtonInformation coord = conf != null ? conf : new ButtonInformation(.1, h[0], false);
			h[0] += .01;
			coordinates.put(category, coord);
			if (buttonOption.isPresent()) {
				((ButtonPositionConfiguration)buttonOption.get()).setButton(category, coord);
			}
		});
		super.init();
	}

	@Override
	public List<ConfigurationOption> getDefaultConfig() {
		List<ConfigurationOption> options = new ArrayList<>();
		options.add(new ButtonPositionConfiguration("petroleum.modmenu.buttons", "positions of mod menu buttons", this));
		return options;
	}

	/**
	 * Update coord.
	 *
	 * @param b the b
	 * @param c the c
	 */
	public static void updateCoord(String b, ButtonInformation c) {
		if (c.x < 0.05) {
			c.x = 0.05;
		}
		if (c.x > .95) {
			c.x = .95;
		}
		if (c.y < 0.05) {
			c.y = 0.05;
		}
		if (c.y > .95) {
			c.y = .95;
		}
		if (coordinates.containsKey(b)) {
			coordinates.replace(b, c);
			Optional<ModuleBase> modmenu = ModInfoProvider.getMod("petroleum.modmenu");
			if (modmenu.isPresent()) {
				Optional<ConfigurationOption> buttonOption = modmenu.get().getConfigOption("petroleum.modmenu.buttons");
				if (buttonOption.isPresent()) {
					((ButtonPositionConfiguration)buttonOption.get()).setButton(b, c);
				}
			}
		}
	}

	@Override
	public void activate(MinecraftClient client) {
		this.toggle();
		if (ConfigManager.getConfig(this.getParent()).get().isModuleEnabled(this.getModuleName())) {
			client.openScreen(new PModMenuScreen());
		} else {
			this.toggle(); //turn off again (hopefully)
			client.openScreen(null);
		}
	}

	@Override
	public Map<KeyBinding, ModuleFunction> getDefaultBindings() {
		Map<KeyBinding, ModuleFunction> binds = new HashMap<>();

		KeyBinding openMenu = new KeyBinding(
				"key.petroleum.togglemodmenu",
				InputUtil.Type.KEYSYM,
				GLFW.GLFW_KEY_RIGHT_CONTROL,
				"category.petroleum"
		);
		binds.put(openMenu, new ModuleFunction("petroleum.modmenu.toggle", this));

		return binds;
	}

	/**
	 * Gets buttons.
	 *
	 * @return the buttons
	 */
	public static Map<String, ButtonInformation> getButtons() {
		return coordinates;
	}
}