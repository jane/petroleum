package pm.j4.petroleum.modules.splash;

import java.util.ArrayList;
import java.util.List;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.option.ConfigurationOption;
import pm.j4.petroleum.PetroleumMod;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.option.BooleanOption;
import pm.j4.kerosene.util.module.option.ConfigurationOption;

/**
 * The type Splash text.
 */
public class SplashText extends ModuleBase {
	/**
	 * Instantiates a new Splash text.
	 */
	public SplashText() {
		super("petroleum",
				"petroleum.splashtext",
				"petroleum.misc",
				false,
				true,
				true);
	}

	@Override
	public List<ConfigurationOption> getDefaultConfig() {
		List<ConfigurationOption> options = new ArrayList<>();
		options.add(new BooleanOption("petroleum.splashtext.active", "Show the main menu version text.", this));
		return options;
	}

	/**
	 * Get string.
	 *
	 * @return the string
	 */
	public static String get() {
		if (PetroleumMod.modData != null) {
			return "Petroleum v" + PetroleumMod.modData.getVersion().getFriendlyString();
		}
		return "Petroleum vUnknown";
	}
}
