package pm.j4.petroleum.util.config;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.config.GlobalConfig;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.option.ConfigurationOption;
import pm.j4.petroleum.util.data.ButtonInformation;

public class ButtonPositionConfiguration extends ConfigurationOption {
	/**
	 * The Button locations.
	 */
	private final Map<String, ButtonInformation> buttonLocations = new HashMap<>();

	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param key         the key
	 * @param description the description
	 * @param parent      the parent
	 */
	public ButtonPositionConfiguration(String key, String description, ModuleBase parent) {
		super(key, description, parent);
	}

	/**
	 * Sets button.
	 *
	 * @param category          the category
	 * @param buttonInformation the button information
	 */
	public void setButton(String category, ButtonInformation buttonInformation) {
		if (buttonLocations.containsKey(category)) {
			buttonLocations.replace(category, buttonInformation);
		} else {
			buttonLocations.put(category, buttonInformation);
		}
	}

	/**
	 * Gets button.
	 *
	 * @param category the category
	 * @return the button
	 */
	public ButtonInformation getButton(String category) {
		if (buttonLocations.containsKey(category)) {
			return buttonLocations.get(category);
		}
		System.out.println("Could not find button of category " + category);
		return null;
	}

	@Override
	public String getStringValue() {
		return "";
	}

	@Override
	public void fromJson(JsonElement e) {
		JsonObject obj = e.getAsJsonObject();
		obj.entrySet().forEach(entry -> {
			ButtonInformation button = ConfigManager.deserializeElement(entry.getValue(), ButtonInformation.class);
			buttonLocations.put(entry.getKey(), button);
		});
	}

	@Override
	public JsonElement toJson() {
		JsonObject obj = new JsonObject();
		buttonLocations.forEach((key, value) -> {
			JsonElement element = ConfigManager.serializeElement(value);
			obj.add(key, element);
		});
		return obj;
	}
}
