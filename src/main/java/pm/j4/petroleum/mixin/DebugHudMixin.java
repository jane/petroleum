package pm.j4.petroleum.mixin;

import java.util.List;
import java.util.Optional;
import net.minecraft.client.gui.hud.DebugHud;
import net.minecraft.client.util.math.MatrixStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import pm.j4.kerosene.util.data.ModInfoProvider;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.option.BooleanOption;
import pm.j4.kerosene.util.module.option.ConfigurationOption;
import pm.j4.petroleum.modules.splash.SplashText;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;

/**
 * The type Debug hud mixin.
 */
@Mixin(DebugHud.class)
public class DebugHudMixin {
	/**
	 * Render text right.
	 *
	 * @param matrices the matrices
	 * @param ci       the ci
	 * @param list     the list
	 */
	@Inject(method = "renderLeftText",
			at = @At(value = "INVOKE_ASSIGN", target = "Lnet/minecraft/client/gui/hud/DebugHud;getLeftText()Ljava/util/List;"),
			locals = LocalCapture.CAPTURE_FAILSOFT)
	protected void renderTextRight(MatrixStack matrices, CallbackInfo ci, List<String> list) {
		Optional<ConfigHolder> config = ConfigManager.getConfig("petroleum");
		Optional<ModuleBase> splashText = ModInfoProvider.getMod("petroleum.splashtext");
		if (config.isPresent() && config.get().isModuleEnabled("petroleum.splashtext")
				&& splashText.isPresent()) {
			Optional<ConfigurationOption> isActive = splashText.get().getConfigOption("petroleum.splashtext.active");
			if(isActive.isPresent() && ((BooleanOption)isActive.get()).getValue()) {
				list.add("[Petroleum] " + SplashText.get() + " loaded");
			}
		}

	}
}
