package pm.j4.petroleum;

import java.util.Optional;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.ModMetadata;
import pm.j4.kerosene.util.config.Module;
import pm.j4.petroleum.modules.list.ModList;
import pm.j4.petroleum.modules.menu.ModMenu;
import pm.j4.petroleum.modules.splash.SplashText;
import pm.j4.petroleum.modules.xray.Xray;
import pm.j4.kerosene.util.config.ConfigManager;


//TODO:
// petroleum module checklist
// [ ] xray (lol)
// [ ] combat stuff. killaura, anti knockback, etc
// [ ] render stuff. tracers, nametags
// [ ] wurst taco. but a fish
// [ ] elytra fly
// [ ] movement stuff. nofall, jesus, speed
// [ ] elytra bhop
// [ ] boatfly
// [ ] anti anti cheat
// [ ] held (not toggled) keybinds (impl in kerosene)

/**
 * The type Petroleum mod.
 */
@Module(SplashText.class)
@Module(ModMenu.class)
@Module(ModList.class)
@Module(Xray.class)
public class PetroleumMod implements ModInitializer {
	/**
	 * The Mod data.
	 */
	public static ModMetadata modData = null;

	@Override
	public void onInitialize() {

		ConfigManager.initConfig("petroleum", PetroleumMod.class);

		// always update mod data
		Optional<ModContainer> modContainer = FabricLoader.getInstance().getModContainer("petroleum");
		modContainer.ifPresent(container -> modData = container.getMetadata());

		try {
			//register mods
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
}
