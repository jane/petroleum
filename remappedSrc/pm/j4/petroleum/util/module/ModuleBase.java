package pm.j4.petroleum.util.module;

import com.google.gson.JsonElement;
import java.util.*;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.TranslatableText;
import pm.j4.petroleum.gui.PModuleConfigEntry;
import pm.j4.petroleum.util.config.ConfigHolder;
import pm.j4.petroleum.util.config.ConfigManager;
import pm.j4.petroleum.util.module.option.ConfigurationOption;

/**
 * The Basis for all mods, used so that modules all have a common activation point and settings.
 */
public abstract class ModuleBase {

	/**
	 * Instantiates a new Module base.
	 * Parameters should be constant across restarts.
	 *
	 * @param name          The name of the module
	 * @param category      the category
	 * @param activatable   Whether a module can be activated, or if it will remain in the state it was upon startup
	 * @param hidden        Whether the module will show up in @link pm.j4.petroleum.modules.menu.ModMenu or the active module list
	 * @param hasConfigMenu whether a button in the configuration menu will show
	 */
	public ModuleBase(String name, String category, boolean activatable, boolean hidden, boolean hasConfigMenu) {
		this.moduleName = name;
		this.category = category;
		this.readableName = new TranslatableText(name);
		this.activatable = activatable;
		this.hidden = hidden;
		this.hasConfigMenu = hasConfigMenu;
		this.moduleOptions = this.getDefaultConfig();
	}

	/**
	 * Init.
	 */
	public void init() {

	}

	/**
	 * Activate. Should be overridden.
	 *
	 * @param client the client
	 */
	public void activate(MinecraftClient client) {
		this.toggle();
	}

	/**
	 * Toggle mod.
	 */
	public void toggle() {
		Optional<ConfigHolder> config = ConfigManager.getConfig();
		config.ifPresent(configHolder -> configHolder.toggleModule(this.moduleName));
	}

	/**
	 * The Module's name.
	 */
	private final String moduleName;

	/**
	 * Gets module name.
	 *
	 * @return the module name
	 */
	public String getModuleName() {
		return this.moduleName;
	}

	/**
	 * The Category.
	 */
	private final String category;

	/**
	 * Gets category.
	 *
	 * @return the category
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * The Readable name.
	 */
	private final TranslatableText readableName;

	/**
	 * Gets readable name.
	 *
	 * @return the readable name
	 */
	public TranslatableText getReadableName() {
		return this.readableName;
	}

	/**
	 * The Activatable.
	 */
	private final boolean activatable;

	/**
	 * Is activatable boolean.
	 *
	 * @return the boolean
	 */
	public boolean isActivatable() {
		return activatable;
	}

	/**
	 * The Hidden.
	 */
	private final boolean hidden;

	/**
	 * Is hidden boolean.
	 *
	 * @return the boolean
	 */
	public boolean isHidden() {
		return hidden;
	}


	/**
	 * The Has config menu.
	 */
	private final boolean hasConfigMenu;

	/**
	 * Configurable boolean.
	 *
	 * @return the boolean
	 */
	public boolean configurable() {
		return hasConfigMenu;
	}

	/**
	 * The Module options.
	 */
	private final Map<String, ConfigurationOption> moduleOptions;

	/**
	 * Gets module configuration.
	 *
	 * @return the module configuration
	 */
	public Map<String, ConfigurationOption> getModuleConfiguration() {
		return moduleOptions;
	}

	/**
	 * Sets config option.
	 * This will fail if the option is not already present in a module.
	 * <p>
	 *
	 * @param key   the key
	 * @param value the value
	 * @return whether the operation was successful.
	 */
	public boolean setConfigOption(String key, JsonElement value) {
		if (moduleOptions.containsKey(key)) {
			moduleOptions.get(key).fromJson(value);
			return true;
		}
		return false;
	}

	/**
	 * Has option boolean.
	 *
	 * @param key the key
	 * @return the boolean
	 */
	public boolean hasOption(String key) {
		return moduleOptions.containsKey(key);
	}

	/**
	 * Gets default config.
	 *
	 * @return the default config
	 */
	protected Map<String, ConfigurationOption> getDefaultConfig() {
		return new HashMap<>();
	}

	/**
	 * Gets config option.
	 *
	 * @param key the key
	 * @return the config option
	 */
	public Optional<ConfigurationOption> getConfigOption(String key) {
		if (moduleOptions.containsKey(key)) {
			return Optional.of(moduleOptions.get(key));
		}
		return Optional.empty();
	}

	/**
	 * Gets config entries.
	 *
	 * @return the config entries
	 */
	public List<PModuleConfigEntry> getConfigEntries() {
		List<PModuleConfigEntry> entries = new ArrayList<>();
		this.getModuleConfiguration().forEach((name, option) -> entries.add(new PModuleConfigEntry(option, new TranslatableText(name))));
		return entries;
	}

	/**
	 * Equals boolean.
	 *
	 * @param other the other
	 * @return the boolean
	 */
	public boolean equals(ModuleBase other) {
		return Objects.equals(this.moduleName, other.getModuleName());
	}
}
