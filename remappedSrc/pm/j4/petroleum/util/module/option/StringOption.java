package pm.j4.petroleum.util.module.option;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

/**
 * The type String value.
 */
public class StringOption extends ConfigurationOption {

	/**
	 * The Value.
	 */
	private String value;

	/**
	 * Instantiates a new String option.
	 *
	 * @param description the description
	 */
	protected StringOption(String description) {
		super(description);
	}

	public String getStringValue() {
		return value;
	}

	@Override
	public void fromJson(JsonElement e) {
		this.value = e.getAsString();
	}

	@Override
	public JsonElement toJson() {
		return new JsonPrimitive(value);
	}
}
