package pm.j4.petroleum.util.module.option;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

/**
 * The type Boolean value.
 */
public class BooleanOption extends ConfigurationOption {
	/**
	 * The Value.
	 */
	private boolean value;

	/**
	 * Instantiates a new Boolean option.
	 *
	 * @param description the description
	 */
	public BooleanOption(String description) {
		super(description);
	}

	@Override
	public String getStringValue() {
		return Boolean.toString(value);
	}

	@Override
	public void fromJson(JsonElement e) {
		this.value = e.getAsBoolean();
	}

	@Override
	public JsonElement toJson() {
		return new JsonPrimitive(value);
	}
}
