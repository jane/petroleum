package pm.j4.petroleum.util.module.option;

import com.google.gson.JsonElement;
import net.minecraft.client.options.KeyBinding;
import pm.j4.petroleum.modules.bindings.BindingInfo;
import pm.j4.petroleum.util.config.ConfigManager;
import pm.j4.petroleum.util.config.GlobalConfig;
import pm.j4.petroleum.util.module.ModuleBase;

/**
 * The type Keybind value.
 */
public class KeybindOption extends ConfigurationOption {

	/**
	 * The Value.
	 */
	private KeyBinding value;
	/**
	 * The Converted value.
	 */
	private BindingInfo convertedValue;

	/**
	 * Instantiates a new Keybind option.
	 *
	 * @param description the description
	 */
	public KeybindOption(String description) {
		super(description);
	}

	@Override
	public String getStringValue() {
		return value.getDefaultKey().getLocalizedText().getString();
	}

	@Override
	public void fromJson(JsonElement e) {
		BindingInfo bindingInfo = ConfigManager.deserializeElement(e, BindingInfo.class);
		this.convertedValue = bindingInfo;
		this.value = GlobalConfig.reconstructBinding(bindingInfo);
	}

	@Override
	public JsonElement toJson() {
		return null;
	}

	/**
	 * From keybind.
	 *
	 * @param bind the bind
	 * @param base the base
	 */
	public void fromKeybind(KeyBinding bind, ModuleBase base) {
		this.value = bind;
		this.convertedValue = GlobalConfig.extractBinding(bind, base);
	}
}
