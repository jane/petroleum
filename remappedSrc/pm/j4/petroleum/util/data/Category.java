package pm.j4.petroleum.util.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pm.j4.petroleum.PetroleumMod;
import pm.j4.petroleum.util.module.ModuleBase;

/**
 * The type Category.
 */
public class Category {
	/**
	 * Gets category map.
	 *
	 * @return the category map
	 */
	public static Map<String, List<ModuleBase>> getCategoryMap() {
		List<ModuleBase> modules = PetroleumMod.getActiveMods();
		Map<String, List<ModuleBase>> categoryMap = new HashMap<>();
		modules.forEach(module -> {
			if (!categoryMap.containsKey(module.getCategory())) {
				List<ModuleBase> m = new ArrayList<>();
				m.add(module);
				categoryMap.put(module.getCategory(), m);
			} else {
				List<ModuleBase> m = categoryMap.get(module.getCategory());
				List<ModuleBase> nm = new ArrayList<>();
				nm.addAll(m);
				nm.add(module);
				categoryMap.replace(module.getCategory(), nm);
			}
		});
		return categoryMap;
	}

	/**
	 * Gets by category.
	 *
	 * @param category the category
	 * @return the by category
	 */
	public static List<ModuleBase> getByCategory(String category) {
		return getCategoryMap().containsKey(category) ? getCategoryMap().get(category) : new ArrayList<>();
	}
}
