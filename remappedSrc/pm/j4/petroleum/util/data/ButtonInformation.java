package pm.j4.petroleum.util.data;

/**
 * The type Button information.
 */
public class ButtonInformation {
	/**
	 * The X.
	 */
	public double x;
	/**
	 * The Y.
	 */
	public double y;
	/**
	 * The Open.
	 */
	public boolean open;

	/**
	 * Instantiates a new Button information.
	 *
	 * @param x    the x
	 * @param y    the y
	 * @param open the open
	 */
	public ButtonInformation(double x, double y, boolean open) {
		this.x = x;
		this.y = y;
		this.open = open;
	}
}
