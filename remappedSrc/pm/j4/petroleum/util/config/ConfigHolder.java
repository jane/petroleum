package pm.j4.petroleum.util.config;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import pm.j4.petroleum.PetroleumMod;
import pm.j4.petroleum.util.module.ModuleBase;

/**
 * The type Config holder.
 */
public class ConfigHolder {
	/**
	 * The Global config.
	 */
	public GlobalConfig globalConfig;
	/**
	 * The Server configs.
	 */
	public Map<String, ServerConfig> serverConfigs;

	/**
	 * Is module enabled boolean.
	 *
	 * @param module the module
	 * @return the boolean
	 */
	public boolean isModuleEnabled(String module) {

		if (!PetroleumMod.isActive(module)) {
			return false;
		}
		if (globalConfig.isEnabled(module)) {
			return true;
		}
		String server = this.getServer();
		if (serverConfigs.containsKey(server)) {
			return serverConfigs.get(server).isEnabled(module);
		}
		return false;
	}

	/**
	 * Gets enabled modules.
	 *
	 * @return the enabled modules
	 */
	public List<ModuleBase> getEnabledModules() {
		List<ModuleBase> modules = PetroleumMod.getActiveMods();
		return modules.stream().filter(module ->
				isModuleEnabled(module.getModuleName())
		).collect(Collectors.toList());
	}

	/**
	 * Gets server.
	 *
	 * @return the server
	 */
	public String getServer() {
		return PetroleumMod.getServerAddress();
	}

	/**
	 * Toggle module.
	 *
	 * @param module the module
	 */
	public void toggleModule(String module) {
		String server = this.getServer();
		if (serverConfigs.containsKey(server)) {
			System.out.println("Toggling module " + module + " on server " + server);
			serverConfigs.get(server).toggleModule(module);
		} else {
			globalConfig.toggleModule(module);
		}
	}

	/**
	 * Disable module.
	 *
	 * @param module the module
	 */
	public void disableModule(String module) {
		String server = this.getServer();
		if (serverConfigs.containsKey(server)) {
			System.out.println("disabling module " + module + " on server " + server);
			serverConfigs.get(server).disableModule(module);
		} else {
			globalConfig.disableModule(module);
		}
	}

	/**
	 * Save module.
	 *
	 * @param module the module
	 */
	public static void saveModule(ModuleBase module) {

	}
}
