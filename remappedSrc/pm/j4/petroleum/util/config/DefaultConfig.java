package pm.j4.petroleum.util.config;

import java.util.Collections;

/**
 * The type Default config.
 */
public class DefaultConfig extends GlobalConfig {
	/**
	 * Instantiates a new Default config.
	 */
	public DefaultConfig() {
		this.enabledModules = Collections.singletonList("petroleum.splashtext");
	}
}
