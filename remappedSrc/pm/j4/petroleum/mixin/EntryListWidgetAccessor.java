package pm.j4.petroleum.mixin;

import net.minecraft.client.gui.widget.EntryListWidget;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

/**
 * The interface Entry list widget accessor.
 */
@Mixin(EntryListWidget.class)
public interface EntryListWidgetAccessor {
	/**
	 * Is render selection boolean.
	 *
	 * @return the boolean
	 */
	@Accessor("renderSelection")
	boolean isRenderSelection();
}
