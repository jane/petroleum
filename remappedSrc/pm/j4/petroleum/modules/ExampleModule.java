package pm.j4.petroleum.modules;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.client.MinecraftClient;
import pm.j4.petroleum.util.module.ModuleBase;
import pm.j4.petroleum.util.module.option.BooleanOption;
import pm.j4.petroleum.util.module.option.ConfigurationOption;

/**
 * The type Example module.
 */
public class ExampleModule extends ModuleBase {
	/**
	 * example mod
	 */
	public ExampleModule() {
		super("petroleum.example",
				"petroleum.misc",
				true,
				false,
				true);
	}

	@Override
	protected Map<String, ConfigurationOption> getDefaultConfig() {
		Map<String, ConfigurationOption> options = new HashMap<>();
		options.put("petroleum.example_b_one", new BooleanOption("example"));
		options.put("petroleum.example_b_two", new BooleanOption("example"));
		return options;
	}

	@Override
	public void activate(MinecraftClient client) {
		System.out.println("Example Mod Keybind Activate");
	}
}
