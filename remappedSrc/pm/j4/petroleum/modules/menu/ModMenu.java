package pm.j4.petroleum.modules.menu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.minecraft.client.MinecraftClient;
import pm.j4.petroleum.gui.PModMenuScreen;
import pm.j4.petroleum.util.config.ConfigManager;
import pm.j4.petroleum.util.data.ButtonInformation;
import pm.j4.petroleum.util.data.Category;
import pm.j4.petroleum.util.module.ModuleBase;

/**
 * The type Mod menu.
 */
public class ModMenu extends ModuleBase {

	/**
	 * The constant coordinates.
	 */
	private static final Map<String, ButtonInformation> coordinates = new HashMap<>();

	/**
	 * Instantiates a new Mod menu.
	 */
	public ModMenu() {
		super("petroleum.modmenu",
				"petroleum.misc",
				true,
				true,
				true);
	}

	// TODO figure out resizing
	//  the number itself changes, so it should just be probably like some onResize bullshit in PModMenuScreen
	@Override
	public void init() {
		Map<String, List<ModuleBase>> categories = Category.getCategoryMap();
		final double[] h = {.1};
		categories.forEach((category, moduleList) -> {
			ButtonInformation conf = ConfigManager.getConfig().isPresent() ?
					ConfigManager.getConfig().get().globalConfig.getButton(category) :
					null;
			ButtonInformation coord = conf != null ? conf : new ButtonInformation(.1, h[0], false);
			h[0] += .01;
			coordinates.put(category, coord);
			if (ConfigManager.getConfig().isPresent()) {
				ConfigManager.getConfig().get().globalConfig.setButton(category, coord);
			}
		});
	}

	/**
	 * Update coord.
	 *
	 * @param b the b
	 * @param c the c
	 */
	public static void updateCoord(String b, ButtonInformation c) {
		if (c.x < 0.05) {
			c.x = 0.05;
		}
		if (c.x > .95) {
			c.x = .95;
		}
		if (c.y < 0.05) {
			c.y = 0.05;
		}
		if (c.y > .95) {
			c.y = .95;
		}
		if (coordinates.containsKey(b)) {
			coordinates.replace(b, c);
			if (ConfigManager.getConfig().isPresent()) {
				ConfigManager.getConfig().get().globalConfig.setButton(b, c);
			}
		}
	}

	@Override
	public void activate(MinecraftClient client) {
		this.toggle();
		if (ConfigManager.getConfig().get().isModuleEnabled(this.getModuleName())) {
			client.openScreen(new PModMenuScreen());
		} else {
			client.openScreen(null);
		}
	}

	/**
	 * Gets buttons.
	 *
	 * @return the buttons
	 */
	public static Map<String, ButtonInformation> getButtons() {
		return coordinates;
	}
}