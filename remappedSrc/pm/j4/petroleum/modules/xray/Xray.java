package pm.j4.petroleum.modules.xray;

import pm.j4.petroleum.util.module.ModuleBase;

/**
 * The type Xray.
 */
public class Xray extends ModuleBase {
	/**
	 * Instantiates a new Xray.
	 */
	public Xray() {
		super("petroleum.xray",
				"petroleum.render",
				true,
				false,
				true);
	}
}
