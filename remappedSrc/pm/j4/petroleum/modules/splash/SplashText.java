package pm.j4.petroleum.modules.splash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import pm.j4.petroleum.PetroleumMod;
import pm.j4.petroleum.gui.PModuleConfigEntry;
import pm.j4.petroleum.util.config.ConfigManager;
import pm.j4.petroleum.util.module.ModuleBase;
import pm.j4.petroleum.util.module.option.BooleanOption;
import pm.j4.petroleum.util.module.option.ConfigurationOption;
import pm.j4.petroleum.util.module.option.KeybindOption;

/**
 * The type Splash text.
 */
public class SplashText extends ModuleBase {
	/**
	 * Instantiates a new Splash text.
	 */
	public SplashText() {
		super("petroleum.splashtext",
				"petroleum.misc",
				false,
				true,
				true);
	}

	@Override
	public List<PModuleConfigEntry> getConfigEntries() {
		List<PModuleConfigEntry> entries = new ArrayList<>();
		ConfigurationOption activeToggle = new BooleanOption("Show the main menu version text:");
		PModuleConfigEntry activeEntry = new PModuleConfigEntry(activeToggle, new LiteralText("Active"));
		entries.add(activeEntry);
		return entries;
	}

	/**
	 * Get string.
	 *
	 * @return the string
	 */
	public static String get() {
		if (PetroleumMod.modData != null) {
			return "Petroleum v" + PetroleumMod.modData.getVersion().getFriendlyString();
		}
		return "Petroleum vUnknown";
	}
}
