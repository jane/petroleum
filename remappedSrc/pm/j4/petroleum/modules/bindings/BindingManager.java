package pm.j4.petroleum.modules.bindings;

import java.util.*;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.TranslatableText;
import org.lwjgl.glfw.GLFW;
import pm.j4.petroleum.PetroleumMod;
import pm.j4.petroleum.gui.PModuleConfigEntry;
import pm.j4.petroleum.util.config.ConfigManager;
import pm.j4.petroleum.util.config.GlobalConfig;
import pm.j4.petroleum.util.module.ModuleBase;
import pm.j4.petroleum.util.module.option.KeybindOption;

/**
 * The type Binding manager.
 */
public class BindingManager extends ModuleBase {
	/**
	 * Instantiates a new Module base.
	 * Parameters should be constant across restarts.
	 */
	public BindingManager() {
		super("petroleum.bindings",
				"petroleum.misc",
				false,
				true,
				true);
	}

	@Override
	public void init() {
		registerBindings();
		super.init();
	}

	@Override
	public List<PModuleConfigEntry> getConfigEntries() {
		List<PModuleConfigEntry> entries = new ArrayList<>();

		Map<KeybindOption, ModuleBase> mapped = new HashMap<>();
		if (ConfigManager.getConfig().isPresent()) {
			Map<KeyBinding, ModuleBase> binds = ConfigManager.getConfig().get().globalConfig.bindings;

			binds.forEach((key, func) -> {
				KeybindOption option = new KeybindOption(func.getModuleName() + " " + func.getCategory());
				option.fromKeybind(key, func);
				mapped.put(option, func);
			});
		}
		mapped.forEach((configEntry, module) -> {
			PModuleConfigEntry entry = new PModuleConfigEntry(configEntry, new TranslatableText(module.getModuleName())) {
				//TODO keybinding. most likely involves mixin to take direct key input
				// look into how keybinding in regular options screen works
				@Override
				public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
					if (this.displayText != null) {
						MinecraftClient.getInstance().textRenderer.drawWithShadow(matrices, displayText, x, y, 0xAAAAAA);
					}
					if (this.option != null) {
						int fontHeight = MinecraftClient.getInstance().textRenderer.fontHeight;
						MinecraftClient.getInstance().textRenderer.drawWithShadow(matrices, "Key Value: " + this.option.getStringValue(), x, y + fontHeight + 4, 0xFFFFFF);
					}
				}
			};
			entries.add(entry);
		});
		return entries;
	}

	/**
	 * Register bindings.
	 */
	private void registerBindings() {
		if (!ConfigManager.getConfig().isPresent()) {
			return;
		}
		GlobalConfig c = ConfigManager.getConfig().get().globalConfig;
		Optional<ModuleBase> mod = PetroleumMod.getMod("petroleum.modmenu");
		if (mod.isPresent() && !c.isBound(mod.get())) {
			//TODO
			// the only explicit keybinding (for now.)
			// once the binding manager has been completed,
			// this should be migrated there, as a default binding
			KeyBinding binding = new KeyBinding(
					"key.petroleum.togglemodmenu",
					InputUtil.Type.KEYSYM,
					GLFW.GLFW_KEY_RIGHT_CONTROL,
					"category.petroleum"
			);
			ConfigManager.getConfig().get().globalConfig.setBinding(binding, mod.get());
		}
	}
}
