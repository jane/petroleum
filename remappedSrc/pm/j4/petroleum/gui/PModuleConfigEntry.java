package pm.j4.petroleum.gui;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.widget.EntryListWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import pm.j4.petroleum.util.module.option.BooleanOption;
import pm.j4.petroleum.util.module.option.ConfigurationOption;
import pm.j4.petroleum.util.module.option.ListOption;

/**
 * The type P module config entry.
 */
public class PModuleConfigEntry extends EntryListWidget.Entry<PModuleConfigEntry> {
	/**
	 * The Option.
	 */
	protected final ConfigurationOption option;
	/**
	 * The Display text.
	 */
	protected final Text displayText;

	/**
	 * Instantiates a new P module config entry.
	 *
	 * @param option the option
	 * @param text   the text
	 */
	public PModuleConfigEntry(ConfigurationOption option, Text text) {
		this.option = option;
		this.displayText = text;
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (this.isMouseOver(mouseX, mouseY)) {
			return true;
		}
		return false;
	}

	@Override
	public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
		if (this.displayText != null) {
			MinecraftClient.getInstance().textRenderer.drawWithShadow(matrices, displayText, x, y, 0xAAAAAA);
		}
		if (this.option != null) {
			//TODO option text box (?)
			// option should be centered or otherwise offset
			// but not extend past the side of the pane
			int fontHeight = MinecraftClient.getInstance().textRenderer.fontHeight;
			MinecraftClient.getInstance().textRenderer.drawWithShadow(matrices, new LiteralText(option.getDescription() + " " + option.getStringValue()), x, y + fontHeight + 4, 0xFFFFFF);
			String className = option.getClass().toString();
			if (className == BooleanOption.class.toString()) {
				// boolean button
			}
			else if (className == ListOption.class.toString()) {
				// handle list
				// TODO: determine whether list options are viable,
				//  considering that it would be easier to split lists into multiple PModuleConfigEntries
			}
			else {
				// string button
			}
		}
	}
}